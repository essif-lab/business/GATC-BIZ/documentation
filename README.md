## GATACA CONNECT Project Summary

For more information, please contact: 

Technical lead: Jose - jose@gataca.io
Business lead: Irene - irene@gataca.io

## Introduction

### About GATACA

GATACA is a full-stack SSI technology provider that offers a comprehensive platform for decentralized identity and access management:

- GATACA Wallet: digital ID wallet for consumers
- GATACA Certify: tools for Trusted Authorities to issue W3C-compliant verifiable credentials
- GATACA Connect: single-sign-on authentication tools for businesses to accept decentralized digital IDs for authentication.

As part of eSSIF-Lab, GATACA has proposed a fundamental evolution of our GATACA Connect module: to open our APIs.

#### About GATACA Connect

GATACA Connect is an API-based self-hosted enterprise software offered to Verifiers that enables the identification and authentication of new and existing users utilizing a decentralized identity wallet with W3C-compliant Verifiable Credentials (VCs).
GATACA Connect offers a web-based administration panel that allows business to configure authentication requirements and package them into a QR code.
GATACA Connect functions are:

- Generate a W3C-compliant DID and associated cryptographic Keypair for Verifiers and publish in any blockchain of the Verifier’s choice.
- Configure a presentation request and display a unique QR code per session containing service access requirements.
- Collect VCs from Identity wallets and validate cryptographic signatures by consulting the corresponding public keys in the corresponding blockchain infrastructure Collect, sign and manage users’s consent objects related to the authorized use of users’ personal information

GATACA Connect benefits to Verifiers (as per current state of the art):

- Easy to deploy API-based SDK with a Web Administration panel
- Blockchain agnostic: currently supporting Hyperledger Fabric, Ethereum, Ethereum Enterprise (with Hyperledger Besu), and Alastria.
- Aligned with eSSIF (EBSI) framework
- OpenID integration
- Integrated with a full-stack technology solution that includes GATACA Wallet (for users) and GATACA Certify (VC issuing SDK)

### eSSIF-Lab Project: open APIs for GATACA Connect

GATACA Connect is an SSI Solution for Verifiers that allows them to request and verify W3C-compliant Verifiable Credentials to authenticate users. GATACA Connect integrates with other GATACA components, such us its GATACA wallet app, its DID resolver or its Schema and Issuer Registries in order to verify DIDs and Verifiable Credentials.

GATACA Connect -and the entire GATACA platform- was designed following global interoperability principles. For this reason, it implements existing W3C standards and provides an interoperability layer to detach the blockchain infrastructure layer from the SSI solution layer. However, the immaturity of the market and the lack of a clear winning provider or interoperability among different implementations makes it difficult for stakeholders to choose a specific vendor to start promoting real use cases, as they fear vendor-lock in situations or waste of resources on loosing proposals.

For this reason, our goal for this Lab is to make GATACA Connect Gataca-agnostic; that is, for it to work with other
wallets and DID resolver providers. For this purpose, GATACA proposes to define open APIs for our GATACA Connect module (a Verifier's
component). The goal of opening the APIs is to allow other providers to integrate their wallets with our verifier in case they wish to, or to build new Verifiers following the same interfaces, allowing our wallet to connect to said verifiers.
The ultimate goal behind this work is to allow clearing the existing binding between the Wallets and Verifiers of any technology provider.

## Summary

### Business Problem

Digital Identity is comprised of a complex ecosystem with three different stakeholders.

Current SSI providers are doing their best to develop full-stack platforms that attend to the needs of all stakeholders at once, as developing technology for one single actor does not solve any real world use case.

Furthermore, all SSI providers recognize the importance of interoperability and standardization in order to make such a profound change in the authentication architecture in the Internet, so most of the organizations tend to abide and support W3C and other upcoming standards. 
However, there is a lack of standardization in the communication protocols between the Verifier and the Holder, so each provider is building their own. Until we solve this problem, Verifiers may suffer vendor lock-in and Holders may need to hold multiple wallets, a situation we want to avoid.

### Technical Solution

A Draft has been redacted exposing the APIs and the protocols implemented internally by the GATACA Connect verifier's
component. Instead of publishing the code on an open-source model, this spec has been redacted on ReSpec, so as to
enabling an easier understanding of all the
internal workings of the module or integrate with it if desired.

Our Spec defines 6 different interfaces for our Verifier component:
- Presentation Exchange
- Consent Managmeent
- Schema registry
- Issuer registry
- DID resolution
- Credential Status Query

An overview of proposed open APIs can be found at our [Verifier Interface Spec](https://gataca-io.github.io/verifier-apis)

## Integration with the eSSIF-Lab Functional Architecture and Community

All the protocols and models implemented try to be as public, standard as accepted as possible. As such, our open APIs
leverage existing standards and specifications from the broad SSI community:

- Decentralized Identifier (DID) as defined in [DID-CORE](https://www.w3.org/TR/did-core/)
- DID resolution as defined in [DID-RESOLUTION](https://w3c-ccg.github.io/did-resolution/)
- Verifier as defined in [VC-DATA-MODEL](https://www.w3.org/TR/vc-data-model/)
- Verifiable Credential (VC) as defined in [VC-DATA-MODEL](https://www.w3.org/TR/vc-data-model/)
- Verifiable Presentation (VP) as defined in [VC-DATA-MODEL](https://www.w3.org/TR/vc-data-model/)

Our open APIs have been presented to the eSSIF-Lab community, as well as to the W3C, DIF and EBSI communities with great
reception. As a side initiative, we've launched the Verifier Universal Interface (VUI), an interoperability group aiming
at evolving our open APIs and defining standard APIs instead that everyone could abide to. This would greatly reduce
interoperability efforts (instead of having to develop a driver for each Verifier that exposes their APIs, all providers
could implement just one standard API compatible with existing and future solutions). To date, 12 organizations are
involved in making VUI possible. 
